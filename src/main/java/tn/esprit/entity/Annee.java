package tn.esprit.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;



@Entity
public class Annee implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private AnneePK idannee;
	private Etudiant Etudiant;
	private Niveau Niveau;
	
	@EmbeddedId
	public AnneePK getIdannee() {
		return idannee;
	}
	public void setIdannee(AnneePK idannee) {
		this.idannee = idannee;
	}
	@ManyToOne
	public Etudiant getEtudiant() {
		return Etudiant;
	}
	public void setEtudiant(Etudiant Etudiant) {
		this.Etudiant = Etudiant;
	}
	@ManyToOne
	public Niveau getNiveau() {
		return Niveau;
	}
	public void setNiveau(Niveau Niveau) {
		this.Niveau = Niveau;
	}

}
