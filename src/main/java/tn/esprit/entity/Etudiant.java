package tn.esprit.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;



@Entity
public class Etudiant implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int  cin ;
	private String nom ;
	private String prenom;
	private String mail;
	private String date_naissance;
	private String addresse;
	private List<Annee> Annees;
	
	public Etudiant(int cin, String nom, String prenom, String mail, String date_naissance, String addresse) {
		super();
		this.cin = cin;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.date_naissance = date_naissance;
		this.addresse = addresse;
	}
	public String getAddresse() {
		return addresse;
	}
	public void setAddresse(String addresse) {
		this.addresse = addresse;
	}
	@Id
	public int getCin() {
		return cin;
	}
	public void setCin(int cin) {
		this.cin = cin;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getDate_naissance() {
		return date_naissance;
	}
	public void setDate_naissance(String date_naissance) {
		this.date_naissance = date_naissance;
	}
	@OneToMany(mappedBy="etudiant")
	public List<Annee> getAnnees() {
		return Annees;
	}
	public void setAnnees(List<Annee> Annees) {
		this.Annees = Annees;
	}
}
