package tn.esprit.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Niveau implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id ;
	private String desc;
	private List<Annee> Annees;
	
	@Id
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	@OneToMany(mappedBy="niveau")
	public List<Annee> getAnnees() {
		return Annees;
	}
	public void setAnnees(List<Annee> Annees) {
		this.Annees = Annees;
	}
}
